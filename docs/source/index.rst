Waypoint
========

.. image:: _static/images/waypoint-logo-default-nobg.png
    :width: 120px
    :target: https://app.waypointhq.com
    :alt: Waypoint logo
    :align: center


Waypoint is a manager-centric toolset to monitor and improve employee engagement. It is used to identify gaps in automate 
key managment tasks.


Getting help
------------

New to Waypoint? We'd like to help!

* Try the How To's section to find quick information about product usage.
* Looking for specific information? Try the :ref:`genindex`.
* Report bugs or issues with Waypoint at support@waypointhq.com



First Steps
-----------

.. toctree::
   :maxdepth: 2
   :caption: First Steps
   :hidden:

   firststeps/intro

:doc:`firststeps/intro`
    Understand what Waypoint is and how it can help you



How To's
--------

.. toctree::
   :maxdepth: 2
   :caption: How To's
   :hidden:

   howtos/add-a-team-member 
   howtos/setup-an-adhoc-meeting
   howtos/setup-a-recurring-meeting
   howtos/Log-notes-about-a-member

:doc:`howtos/add-a-team-member`
    Add your team members

:doc:`howtos/setup-an-adhoc-meeting`
    Create your first meeting with a team member

:doc:`howtos/setup-a-recurring-meeting`
    Learn to automate meeting schedulings

:doc:`howtos/Log-notes-about-a-member`
    Log Notes at multiple places


Features
--------

.. toctree::
   :maxdepth: 2
   :caption: Features
   :hidden:

   features/meetings
   features/recurring
   features/forms
   features/integrations
   features/funfacts
   features/psychometrics

:doc:`features/meetings`
    Learn about the engagement enabler

:doc:`features/recurring`
    Learn about the recurring meetings

:doc:`features/forms`
    Gather intel by creating custom forms / surveys on-the-fly

:doc:`features/integrations`
    Integrate Meeting Events with your favourite Calendar.

:doc:`features/funfacts`
    small intro about Funfacts

:doc:`features/psychometrics`
    Define Psychometrics

