Setup an Adhoc Meeting
======================

1. From the Team Dashboard, click on a particular Employee icon.
2. On the top right, click on *Actions* pull down button.
3. Click on *Setup a new meeting*


This would open a new meeting scheduler widget.

Step 1: Choose Meeting Specifics
--------------------------------


Step 2: Attach a Form to this meeting
-------------------------------------



Step 3: Confirm to schedule the meeting
---------------------------------------