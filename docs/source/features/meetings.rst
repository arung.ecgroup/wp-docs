Meetings
========

Meetings allows a manager and an employee to track their conversations and engagements. 

.. hint:
    Both Managers and Employees enjoy the following features and the content is exclusive to them.

.. index:: Notes

**Notes**

1. Manager and Employee can write notes immedietely after scheduling the meeting. 
2. Notes supports markup and the formatting tools provided could be used to make it presentable.


.. index:: Action Items

**Action Items**

