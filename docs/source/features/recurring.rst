Recurring Meetings
==================

Recurring Meeting allows a manager to engage employee in a repeated fashion.

Waypoint has an ability to schedule a meeting in a repeated fashion on a regular basis.Recurring Meeting has recurrence and range options.
* Recurrence: For Recurrence you will specify the recurrence option (daily, weekly, monthly, or yearly) and frequency.
* Frequency: Each recurrence option has difference frequency options.  Select the one best suited to your needs.(Every month 5th,Every Mondays,ect…)
* Ending: The recurrence can have have no end date, a specified end date, or a specified number of meetings.

.. hint:
    Both Managers and Employees enjoy the following features and the content is exclusive to them.

.. index:: Notes

**Notes**

1. Manager and Employee can write notes immedietely after scheduling the meeting. 
2. Notes supports markup and the formatting tools provided could be used to make it presentable.


.. index:: Action Items

**Action Items**
